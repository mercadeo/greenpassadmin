  # encoding: utf-8
class Api::V1::TokensController  < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def index
    @user=User.find_by_authentication_token(params[:token])
    
    if @user.present?
      #logger.info(“Token wasnot found.”)
      render :status=>200, :json=> @user.to_builder.target!
    else
      render :status=>404, :json=>{:message=>"Token inválido, no se encuentra el usuario asociado"}
    end
  end
  
  def create
    email = params[:email]
    password = params[:password]
    if request.format != :json
      render :status=>406, :json=>{:message=>"The request must be json"}
      return
     end

    if email.nil? or password.nil?
       render :status=>400,
              :json=>{:message=>"The request must contain the user email and password."}
       return
    end

    @user=User.find_by_email(email.downcase)

    if @user.nil?
      logger.info("User #{email} failed signin, user cannot be found.")
      render :status=>401, :json=>{:message=>"User #{email} failed signin, user cannot be found."}
      return
    end

    if @user.active?
      # http://rdoc.info/github/plataformatec/devise/master/Devise/Models/TokenAuthenticatable
      @user.ensure_authentication_token!

      if not @user.valid_password?(password)
        logger.info("User #{email} failed signin, password \"#{password}\" is invalid")
        render :status=>401, :json=>{:message=>"Invalid email or password."}
      else
        @user.isLoggedIn = true
        @user.authenticateDateUpdater(request.remote_ip)
        render :status=>200, :json=>{:token=>@user.authentication_token, :change_password=>@user.reset_password_token.present?}
      end
    else 
      logger.info("User #{email} failed signin, must activate account")
      render :status=>401, :json=>{:message=>"User #{email} failed signin, must activate account", activate: true}
    end
  end

  def destroy
    @user=User.find_by_authentication_token(params[:id])
    if @user.nil?
      #logger.info(“Token wasnot found.”)
      render :status=>404, :json=>{:message=>"Invalid token. No session found"}
    else
      @user.isLoggedIn = false
      @user.reset_authentication_token!
      render :status=>200, :json=>{:token=>params[:id]}
    end
  end

end