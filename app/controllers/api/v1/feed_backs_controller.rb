class Api::V1::FeedBacksController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :set_feedback, only: [:show, :edit, :update, :destroy]

  def create

    user = User.find_by_authentication_token(params[:token])
    if user.present?
      @feed_back = FeedBack.new(feedback_params)
      @feed_back.user = user
      if @feed_back.save
        render json: { :message=> "Mensaje enviado exitosamente"}, status: 201
      else
        render json: { :message=> "Mensaje no enviado, inténtelo nuevamente"},
             status: 500
      end
    else
      render json: { :message=> "Sesión Inválida, Inicie sesión nuevamente"},
               status: 404
    end
  end

  private

    def set_feedback
      @category = FeedBack.find(params[:id])
    end

    def feedback_params
      params.require(:feed_back).permit(:title, :description)
    end
end
