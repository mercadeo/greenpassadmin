require 'securerandom'
class Api::V1::ActivateAccountController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def index
    user = User.find_by_email(params[:email])
    if user.present?
      if user.active?
        render json: { :message=> "El usuario #{user.email}, ya se encuentra activo. Ingresa con tus credenciales o ponte en contacto con el administrador",
          back_home: true},status: 404  
      else
        if user.user_attribute.present? 
          if user.valid_carnet?(params[:carnet])
            begin
              user.send_api_reset_password_instructions
              render json: { :message=> "Hemos enviado la contraseña temporal a su correo #{user.email}. Ingresa con tu cuenta y el código enviado", back_home: true},
                status: 200
              rescue => error
                render json: { :message=> error.inspect},
                  status: 404
            end
          else
            render json: { :message=> "Número de empleado inválido, verifica que el número corresponda al empleado #{user.email}"},
               status: 404
          end   
        else
          render json: { :message=> "Número de empleado asociado a #{user.email} no registrado, ponte en contacto con el administrador"},
                  status: 404
        end
      end
    else
      render json: { :message=> "Usuario #{params[:email]} inválido, no se encuentra registrado"},
             status: 404
    end
  end
end