require 'securerandom'
class Api::V1::ResetPasswordsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def index
    user = User.find_by_email(user_params[:email])
    if user.present?
      begin
        user.send_api_reset_password_instructions
        render json: { :message=> "Hemos enviado la contraseña temporal a su correo #{user.email}. Ingresa con tu cuenta y el código enviado", back_home: true},
             status: 201
        rescue => error
          render json: { :message=> "Ha ocurrido un error, inténtelo nuevamente"},
            status: 404
      end   
    else
      render json: { :message=> "Usuario #{params[:email]} inválido, no se encuentra registrado"},
             status: 404
    end
  end

  private
  def user_params
    params.require(:user).permit(:email)
  end
end