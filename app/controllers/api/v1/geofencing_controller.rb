class Api::V1::GeofencingController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def index
      render :status=>200, :json=> Store.json_builder.target!
  end
end