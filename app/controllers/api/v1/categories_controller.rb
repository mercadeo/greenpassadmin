class Api::V1::CategoriesController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    user = User.find_by_authentication_token(params[:token])
    if user.present?
      @categories = Category.joins(:promotions)
      .where('"stores"."status" = ? and "stores"."expireDate" >= ? and end_date >= ? ', true, Date.today, Date.today)
      .select("categories.*").group("categories.id")
    else
      render status: 404
    end

  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    user = User.find_by_authentication_token(params[:token])
    if user.present?
      @category.count += 1
      @category.save
      @promotions = @category.active_promotions
    else
      render status: 404
    end
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params[:category]
    end
end
