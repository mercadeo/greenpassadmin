class Api::V1::ChangeAvatarController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def update
    puts "ESTOS SON LOS PARAMETROS #{params}"
    user = User.find_by_authentication_token(params[:id])
    if user.present? 
      if params[:file].present?
        begin
          user.avatar = params[:file]
          user.save!
          #puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<  #{convert_data_uri_to_upload(params[:file])}"

          #user.avatar=params[:password]
          #user.password_confirmation=params[:password_confirmation]
          #user.save!
          #logger.info("User #{user.email}, password \"#{user.password}\"")
          render json: { :message=> "Avatar actualizado exitosamente"},
            status: 201
          rescue ActiveRecord::RecordInvalid => error
            puts "#{error.record.errors.messages} ++++ #{params[:file]}"
            render json: { :message=> error.record.errors.messages[:avatar]},
              status: 500
        end   
      else
        render json: { :message=> "Valores inválidos, agregue nuevamente su avatar"},
               status: 404
      end
    else
      render json: { :message=> "Sesión Inválida, Inicie sesión nuevamente"},
               status: 404
    end
  end 
end