class Api::V1::ChangePasswordController < ApplicationController
  skip_before_filter :verify_authenticity_token
  respond_to :json
  def update
    user = User.find_by_authentication_token(params[:id])
    if user.present? 
      if params[:password].present?
        begin
          user.password=params[:password]
          user.password_confirmation=params[:password_confirmation]
          user.save!
          logger.info("User #{user.email}, password \"#{user.password}\"")
          render json: { :message=> "Contraseña actualizada correctamente"},
            status: 201
          rescue => error
            render json: { :message=> "Valores inválidos, ingrese su nueva contraseña."},
              status: 404
        end   
      else
        render json: { :message=> "Ingrese su nueva contraseña, el campo es requerido."},
               status: 404
      end
    else
      render json: { :message=> "Sesión Inválida, Inicie sesión nuevamente"},
               status: 404
    end
  end
end