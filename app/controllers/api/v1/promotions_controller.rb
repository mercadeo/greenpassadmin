class Api::V1::PromotionsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :set_promotion, only: [:show, :edit, :update, :destroy]

  def show
    user = User.find_by_authentication_token(params[:token])
    if user.present?
      @promotion.count += 1
      @promotion.save
      respond_to do |format|
        format.json { render json: "OK", status: 200 }
      end
    else
      render status: 404
    end
  end

  private
    def set_promotion
      @promotion = Promotion.find(params[:id])
    end
end
