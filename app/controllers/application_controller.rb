class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  #before_action :authenticate_user!  # if using devise with user devise controller and force login befor action
  #before_action :set_current_user # if using devise with user devise controller
  def access_denied(exception)
    #flash[:error] = "Access denied!"
    redirect_to root_path, :alert => exception.message
  end
  def redirect_to(*args)
    flash.keep
    super
  end
  protected
  def set_current_user
  	#if user_signed_in? # if using devise with user devise controller
  	#	@current_user = current_user
  	#end
  end
end
