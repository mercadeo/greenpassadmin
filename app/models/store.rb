class Store < ActiveRecord::Base
  	belongs_to :category
  	has_many :branch_offices, dependent: :destroy
  	has_many :promotions, dependent: :destroy
  	accepts_nested_attributes_for :branch_offices, allow_destroy: true
  	mount_uploader :logo, LogoUploader
	validates :logo, 
    :presence => true, 
    :file_size => { 
      less_than_or_equal_to: 50.kilobytes
    } 
	
	validates :title, presence: true, uniqueness: true
	validates :store_description, presence: true
	validates :category, presence: true
	validates :longitud, presence: true, numericality: true
	validates :latitud, presence: true, numericality: true
	validates :expireDate, presence: true

	def display_name 
		self.title
	end
	def valid_date
		Date.today <= self[:expireDate].to_date 
	end
	def image_source
		self.logo.url
	end
	def is_active?
		return status
	end
	def self.expireList
		self.where("'stores.expireDate' >= ?", Date.today - 1.month).order(:expireDate).last(5)
	end
	def self.json_builder 
		Jbuilder.new do |comercio|
			comercio.comercios self.where('"stores"."status" = ? and "stores"."expireDate" >= ? ', true, Date.today).each do |store|
				comercio.comercio store.to_builder
			end
		end
	end
	def to_builder
		item = Jbuilder.new do |comercio|
			comercio.promociones promotions.where('end_date >= ?', Date.today).each do |promotion|
		        comercio.promocion do 
		        	comercio.nombre_comercio title
			        comercio.latitud latitud
			        comercio.longitud longitud
			        comercio.descuento_promocion promotion.discount
			        comercio.descripcion_promocion promotion.title
			        comercio.sucursales branch_offices.each do |branch|
			        	comercio.sucursal branch.to_builder
			        end
			    end
		    end
	  	end
	end
end
