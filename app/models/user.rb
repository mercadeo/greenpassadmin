require 'token_authenticatable'
class User < ActiveRecord::Base
  mount_uploader :avatar, AvatarUploader
  has_one :user_attribute, dependent: :destroy
  has_many :feed_backs
  attr_accessor :skip_password_validation
  accepts_nested_attributes_for :user_attribute, allow_destroy: true
  validates :avatar, 
  :file_size => { 
    less_than_or_equal_to: 2048.kilobytes
  } 
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  COMPANIES = [
    "BANCO LAFISE BANCENTRO",
    "SEGUROS LAFISE",
    "ALMACENADORA LAFISE",
    "LAFISE VALORES",
    "LAFISE INVESTMENT",
    "FUNDACIÓN ZAMORA TERÁN"
  ]

  include TokenAuthenticatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable
  def display_name 
    self.email
  end
  def active?
    self.active
  end
  def valid_carnet?(carnet)
    carnet_validation(carnet)
  end
  def send_api_reset_password_instructions
    token = set_api_reset_password_token
    send_api_reset_password_instructions_notification(token)
    token
  end
  def generatePass
    SecureRandom.hex(6)
  end
  def image_source
    self.avatar.url
  end
  def errorAlertCSV
    error=""
    self.errors.messages.each do |message|
      error+="<li>#{message}</li>"
    end
    return error
  end
  def to_builder
    Jbuilder.new do |usuario|
        usuario.name "#{user_attribute.first_name} #{usuario.last_name user_attribute.last_name}"
        usuario.company_name user_attribute.company_name
        usuario.carnet user_attribute.carnet
        usuario.cedula user_attribute.cedula
        usuario.employee_start_date user_attribute.employee_start_date
        usuario.image_source image_source
    end
  end
  def self.loggedInUsers
    self.where(:isLoggedIn => true).count
  end
  def self.activeUsers
    self.where(:active => true).count
  end
  def self.noActiveUsers
    self.count - self.where(:active => true).count
  end
  def self.recent_auth_access
    all.where("current_sign_in_at is not null").order(:current_sign_in_at).last(5).reverse
  end
  protected

  def password_required?
    return false if skip_password_validation
    super
  end

  def carnet_validation(carnet)
    if self.user_attribute.present? 
      self.user_attribute.carnet == carnet
    else 
      false  
    end
  end

  def set_api_reset_password_token
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)
    randomstring = SecureRandom.hex(6)
    self.password = randomstring
    self.active = true
    self.save
    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    self.save(validate: false)
    #raw
    randomstring
  end
  def send_api_reset_password_instructions_notification(token)
    send_api_devise_notification(:reset_password_instructions, token, {})
  end
  def clear_api_reset_password_token
    self.reset_password_token = nil
    self.reset_password_sent_at = nil
  end
  def send_api_devise_notification(notification, *args)
    if new_record? || changed?
      pending_notifications << [notification, args]
    else
      ApiMailer.send(notification, self, *args).deliver
    end
  end
end
