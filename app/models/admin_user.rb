class AdminUser < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, 
        :recoverable, :rememberable, :trackable, :validatable
    def self.recent_auth_access
    	all.where("current_sign_in_at is not null").order(:current_sign_in_at).last(5).reverse
    end
end
