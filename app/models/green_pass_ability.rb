class GreenPassAbility
  include CanCan::Ability

  def initialize(user)

   
    if user.id == 1
    	can :manage, AdminUser
    	can :manage, :all
    else 
    	can [:read,:update], AdminUser, :id => user.id
    	can [:read,:update, :create], [User, Promotion, Category, BranchOffice, Store]
      cannot [:create], FeedBack
      can [:read, :update], FeedBack
      cannot [:destroy], [FeedBack, User]
    end
    
    can :read, ActiveAdmin::Page, :name => "Dashboard"
  end

end