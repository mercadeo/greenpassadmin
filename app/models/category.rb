class Category < ActiveRecord::Base
	mount_uploader :image, CategoryImageUploader
	has_many :stores
	has_many :promotions, through: :stores

   	validates :title, presence: true, uniqueness: true
	validates :description, presence: true
	validates :image, 
    :presence => true, 
    :file_size => { 
      less_than_or_equal_to: 10.kilobytes
    } 
	
    def active_promotions
    	self.promotions.where('"stores"."status" = ? and "stores"."expireDate" >= ? and end_date >= ? ', true, Date.today, Date.today)
    end
	def display_name 
		self.title
	end

	def is_published?
		return status
	end
	def image_source
		self.image.url
	end
	def self.collection
		all.map {|u| [u.display_name, u.id]}
	end
	def self.top_categories
		self.where("categories.count > ?",0).order(count: :desc).first(5)
	end

end
