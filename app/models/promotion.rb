class Promotion < ActiveRecord::Base
  	belongs_to :store

  	has_one :category, through: :store 
  	mount_uploader :image, PromotionImageUploader
	

  	validates :title, presence: true, uniqueness: true
  	validates :description, presence: true
  	validates :conditions, presence: true
  	validates :store, presence: true
  	validates :discount, presence: true, numericality: true
    validates :start_date, presence: true
  	validates :end_date, presence: true
  	validate :expires_validation
  	validates :image, 
	:presence => true, 
	:file_size => { 
	  less_than_or_equal_to: 200.kilobytes
	} 
	def valid_promotion
		Date.today >= self[:start_date].to_date || Date.today <= self[:end_date].to_date 
	end
	def to_builder
		if valid_promotion
			Jbuilder.new do |promocion|
				promocion.(self, :title, :discount)
			end
		end
  	end
	def expires_validation
	  if (self[:end_date].present? && self[:start_date].present?) && (self[:end_date] < self[:start_date])
	    errors[:start_date] << I18n.t("active_admin.promotion.expires_date", end_date: self[:end_date]) if self.start_date_changed?
	    errors[:end_date] << I18n.t("active_admin.promotion.expires_date", end_date: self[:end_date]) if self.end_date_changed?
	    return false
	  else
	    return true
	  end
	end
	def self.top_promotions
		self.where("promotions.count > ?",0).order(count: :desc).first(5)
	end
	def display_name
		self.title
	end
	def image_source
		self.image.url
	end
	
end
