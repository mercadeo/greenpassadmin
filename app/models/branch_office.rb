class BranchOffice < ActiveRecord::Base
  	belongs_to :store

  	validates :title, presence: true, uniqueness: true
	validates :longitud, presence: true, numericality: true
	validates :latitud, presence: true, numericality: true
	
	def to_builder
    	Jbuilder.new do |sucursal|
    		sucursal.(self, :latitud, :longitud, :title)
    	end
  	end
end
