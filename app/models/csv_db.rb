require 'csv'
class CsvDb
  class << self
    def convert_save(model_name, nested_model_name, nested_index, csv_data)
      isUser = false
      csv_file = csv_data.read
      begin
        csv = CSV.parse(csv_file)
      rescue
        return false
      end
      avoidKeyHash = ["id", "encrypted_password", 
            "authentication_token", "reset_password_token", 
            "reset_password_sent_at", "remember_created_at", 
            "sign_in_count", "current_sign_in_at", "last_sign_in_at", 
            "current_sign_in_ip", "last_sign_in_ip", "created_at", 
            "updated_at", "avatar", "active"]
      avoidNestedKeyHash = ["id", "user_id", "created_at", "updated_at"]
      if csv.present?
        csv.each_with_index do |row, i|
          next if i == 0
          puts ">>>>>>>>>>>>>>>>>>>>>>>>>>>>> #{i}"
          target_model = model_name.classify.constantize
          nested_model = nested_model_name.classify.constantize
          new_object = target_model.new
          if model_name == "user"
            new_object.skip_password_validation = true
            new_object.password = new_object.generatePass
            new_object.active = false
            isUser = true
            new_object.user_attribute = nested_model.new
          end
          column_iterator = -1
          target_model.column_names.each do |key|
            unless (isUser && avoidKeyHash.include?(key)) || (key == "ID")
              column_iterator += 1
              puts "column_iterator >= nested_index #{column_iterator >= nested_index} #{nested_index} #{column_iterator}"
              next if column_iterator >= nested_index
              value = row[column_iterator]
              puts "#{key} --- #{row[column_iterator]}"
              new_object.send "#{key}=", value    
            end
          end
          column_iterator = nested_index
          nested_model.column_names.each do |key|
            unless (avoidNestedKeyHash.include?(key)) || (key == "ID")
              puts "column_iterator < nested_index #{column_iterator < nested_index} #{nested_index} #{column_iterator}"
              value = row[column_iterator]
              puts "#{key} --- #{row[column_iterator]}"
              new_object.user_attribute.send "#{key}=", value
              column_iterator += 1
            end
          end
          begin
            new_object.save!
          rescue
            #raise ActiveRecord::Rollback, "El registro es inválido, verificar valores en csv"
            if new_object.errors.present?
              puts "Saliendo"
              return new_object
            end
          end
        end
        return true
      else
        
      end
    end
  end
end