class FeedBack < ActiveRecord::Base
  belongs_to :user
  validates :title, presence: true
  validates :description, presence: true
  
  def self.last_comments
  	self.where(:read => false).order(:created_at).first(5)
  end

end
