class UserAttribute < ActiveRecord::Base
	belongs_to :user

	validates :employee_start_date, presence: true
	validates :first_name, presence: true
	validates :last_name, presence: true
	validates :carnet, presence: true
	validates :cedula, presence: true
	validates :company_name, presence: true
	validate :start_date_employee
	def start_date_employee
		if self[:employee_start_date].present? && self[:employee_start_date] > Date.today
			errors[:employee_start_date] << I18n.t("active_admin.user_attribute.min_date") if self.employee_start_date_changed?
		  	return false
		else
		  	return true
	end
end
end
