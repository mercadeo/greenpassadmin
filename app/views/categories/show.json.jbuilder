json.extract! @category, :id, :created_at, :updated_at
json.array!(@promotions) do |promotion|
  json.comercio do
    json.nombre promotion.store.title
    json.logo promotion.store.image_source
  end
  json.promocion do
    json.id promotion.id
    json.nombre promotion.title
    json.descripcion promotion.description
    json.descuento promotion.discount
    json.condiciones promotion.conditions
    json.imagen promotion.image_source
  end
end