ActiveAdmin.register User do
	menu label: proc{ I18n.t("active_admin.user.display_name") }
	permit_params :email, :password, :password_confirmation, :avatar, :active, :user_attribute_attributes => [:first_name, :last_name, :carnet, :cedula, :company_name, :employee_start_date ]
	controller do
		def new
			@user = User.new
			@user.build_user_attribute
		end
		def edit 
			@user = User.find(params[:id])
			if @user.user_attribute.blank?
				@user.build_user_attribute
			end
		end
		def create 
			
			@user = User.new(permitted_params[:user])
			@user.skip_password_validation = true
			@user.password = @user.generatePass
			if @user.save
				redirect_to admin_user_path(@user), notice: 'Usuario Creado Exitosamente.' 
			else
				render action: "new"
			end
		end
		def scoped_collection
			super.includes :user_attribute # prevents N+1 queries to your database
		end 
	end

	

	action_item :only => :index do
		link_to 'Upload CSV', :action => 'upload_employee_csv'
	end

	collection_action :upload_employee_csv do
		render "admin/csv/upload_employee_csv"
	end

	collection_action :import_employee_csv, :method => :post do
		if params[:csv].present?
			object = CsvDb.convert_save("user","user_attribute", 1, params[:csv][:file])
			if object.class.name == "User" && object.errors.present?
				flash[:error] = ["Error en la carga, el registro relacionado al correo #{object.email} es inválido."]
	      		flash[:warning] = object.errors.full_messages
				render "admin/csv/upload_employee_csv" #, alert: "Error en la carga, el registro relacionado al correo #{object.email} es inválido. Validar sus atributos y que no exista actualmente en los registros"
			else
				if object
					redirect_to admin_users_path, notice: "CSV imported successfully!" 
				else
					flash[:error] = ["Seleccione el archivo con el formato válido."]
					render "admin/csv/upload_employee_csv"
				end
			end
		else 
			flash[:error] = ["Seleccione el archivo con el formato válido."]
			render "admin/csv/upload_employee_csv"
		end
	end

	filter :email
	filter :active
	config.batch_actions = true
	index do
		selectable_column
		column :email
		column :name, sortable: "user_attributes.first_name" do |user|
	      "#{user.user_attribute.first_name} #{user.user_attribute.last_name}" if user.user_attribute.present?
	    end
	    column :carnet, sortable: "user_attributes.carnet" do |user|
	    	user.user_attribute.carnet if user.user_attribute.present?
	    end 
	    column :empresa, sortable: "user_attributes.company_name" do |user|
	    	user.user_attribute.company_name if user.user_attribute.present?
	    end 
		column :current_sign_in_at
		column :last_sign_in_at
		column :active
		actions
	end

	show do
		panel "Usuario" do
	      attributes_table_for user do
	        row(I18n.t("active_admin.user.email")) { |user| user.email}
	        row(I18n.t("active_admin.user.created_at")) { |user| user.created_at}
	      	row(I18n.t("active_admin.user.active")) { |b| status_tag b.active }
	      	row(I18n.t("active_admin.user.is_logged_in")) { |b| status_tag b.isLoggedIn }
	      end
	    end
	    panel "Detalle de Usuario" do
	      attributes_table_for user.user_attribute do
	        row(I18n.t("active_admin.user.name")) { |user| "#{user.first_name} #{user.last_name}"}
	        row(I18n.t("active_admin.user.carnet")) { |user| user.carnet}
	        row(I18n.t("active_admin.user.cedula")) { |user| user.cedula}
	      	row(I18n.t("active_admin.user.company")) { |user| user.company_name}
	      	row(I18n.t("active_admin.user.start_date")) { |user| user.employee_start_date.strftime("%b - %Y")}
	      end
	    end
	end

	form do |f|
		f.semantic_errors *f.object.errors.keys
	    f.inputs I18n.t("active_admin.user.employee_info") do
			f.semantic_fields_for :user_attribute do |emp|
				emp.input :first_name
				emp.input :last_name
				emp.input :carnet
				emp.input :company_name, :as=>:select, :collection => User::COMPANIES
				emp.input :cedula
				emp.input :employee_start_date, start_year: Date.today.year, end_year: 1990, discard_day: true, order: [:month, :year]
			end
			f.input :email
			f.input :password if !f.object.new_record?
			f.input :password_confirmation if !f.object.new_record?
			f.input :active if !f.object.new_record?
			f.input :avatar, as: :file

		end
	    f.actions
  	end
end