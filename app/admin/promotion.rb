ActiveAdmin.register Promotion do
	menu false
	permit_params :title, :description, :store_id, :image, :start_date, :end_date, :discount, :conditions


	filter :title, :label => I18n.t("active_admin.promotion.title")
	filter :store, :label => I18n.t("active_admin.store.display_name")
	filter :start_date, :label => I18n.t("active_admin.promotion.start_date")
	filter :end_date, :label => I18n.t("active_admin.promotion.end_date")
	filter :discount, :label => I18n.t("active_admin.promotion.discount")
	controller do
	  def scoped_collection
	    super.includes :store # prevents N+1 queries to your database
	  end
	end
	index do
		#selectable_column
		#id_column
		column I18n.t("active_admin.category.title"), :title
		column I18n.t("active_admin.store.display_name"), :store, sortable: "stores.title"
		column I18n.t("active_admin.promotion.start_date"), :start_date
		column I18n.t("active_admin.promotion.end_date"), :end_date
		column I18n.t("active_admin.promotion.discount"), :discount
	actions
	end

	show do
		panel "Category" do
	      attributes_table_for promotion do
	        row(I18n.t("active_admin.promotion.title")) { |promotion| promotion.title}
	        row(I18n.t("active_admin.promotion.description")) { |promotion| promotion.description.html_safe}
	        row(I18n.t("active_admin.promotion.image")) { |promotion| image_tag(promotion.image_source, :style=>"max-width: 300px")}
	        row(I18n.t("active_admin.promotion.title")) { |promotion| promotion.title}
	        row(I18n.t("active_admin.promotion.discount")) { |promotion| status_tag(promotion.discount, :class=>"red")}
			row(I18n.t("active_admin.promotion.start_date")) { |promotion| promotion.start_date}
	        row(I18n.t("active_admin.promotion.end_date")) { |promotion| promotion.end_date}
	        row(I18n.t("active_admin.promotion.conditions")) { |promotion| promotion.conditions.html_safe}
	        row(I18n.t("active_admin.store.display_name")) { |promotion| promotion.store}
	      end
	    end
	end
	form do |f|
		f.semantic_errors *f.object.errors.keys
	    f.inputs "Detalle de Promoción" do
	      f.input :store, :label => I18n.t("active_admin.store.display_name")	
	      f.input :title, :label => I18n.t("active_admin.promotion.title")
	      f.input :description, :label => I18n.t("active_admin.promotion.description"), as: :wysihtml5, commands: [ :bold, :italic, :underline, :ul, :ol, :outdent, :indent, :source ], blocks: [ :h1, :h2, :h3, :h4, :h5, :h6, :p]
	      f.input :discount, :label => I18n.t("active_admin.promotion.discount")
	      f.input :start_date, :label => I18n.t("active_admin.promotion.start_date"), :start_year => Time.now.year
	      f.input :end_date, :label => I18n.t("active_admin.promotion.end_date"),:start_year => Time.now.year
	      f.input :conditions, :label => I18n.t("active_admin.promotion.conditions"),  as: :wysihtml5, commands: [ :bold, :italic, :underline, :ul, :ol, :outdent, :indent, :source ], blocks: [ :h1, :h2, :h3, :h4, :h5, :h6, :p]
	      f.input :image, :label => I18n.t("active_admin.promotion.image"), :as => :file
	    end
	    f.actions
  	end
end