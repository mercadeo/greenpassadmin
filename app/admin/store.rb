ActiveAdmin.register Store do
	menu false
	permit_params :title, :store_description, :category_id, :logo, :status, :latitud, :longitud, :expireDate, branch_offices_attributes: [:id, :title, :longitud, :latitud, :_destroy]
	filter :title
	filter :category
	filter :store_description
	filter :status, label: I18n.t("active_admin.category.published")

	index do
		#selectable_column
		#id_column
		column I18n.t("active_admin.store.title"), :title, sortable: true
		#column I18n.t("active_admin.store.store_description"), :store_description
		column I18n.t("active_admin.store.category"), :category_title, sortable: true
		column I18n.t("active_admin.store.branch_offices"), :sucursal_count, sortable: true
		column I18n.t("active_admin.store.expireDate") do |store|
			span :class=> store.valid_date ? "status_tag ok" : "status_tag error" do
				localize(store.expireDate, :format => :long)
			end
		end
		column I18n.t("active_admin.store.active"), :status
		actions
	end
	controller do
	  def scoped_collection
	    #super.includes [:category, :branch_offices] # prevents N+1 queries to your database
	    super.joins("JOIN categories on categories.id = stores.category_id LEFT JOIN branch_offices on branch_offices.store_id = stores.id")
	    	.select('stores.*, categories.title as category_title, count(branch_offices.*) as sucursal_count')
	    	.group("stores.id, categories.id")
	  end
	end

	show do
		panel I18n.t("active_admin.store.display_name") do
	      attributes_table_for store do
	        row(I18n.t("active_admin.store.title")) { |store| store.title}
	        row(I18n.t("active_admin.store.store_description")) { |store| store.store_description}
	        row(I18n.t("active_admin.store.category")) { |store| store.category.title}
	        row(I18n.t("active_admin.store.latitud")) { |store| store.latitud}
	        row(I18n.t("active_admin.store.longitud")) { |store| store.longitud}
	        row(I18n.t("active_admin.store.logo")) { |store| image_tag(store.image_source, :style=>"max-width: 300px")}
	      	row(I18n.t("active_admin.store.expireDate")) { |b| b.expireDate }
	      	row(I18n.t("active_admin.store.active")) { |b| status_tag b.is_active? }
	      	
	      end
	    end
	end

	form do |f|
		f.semantic_errors *f.object.errors.keys
	    f.inputs I18n.t("active_admin.store.details") do
	      f.input :title, :label => I18n.t("active_admin.store.title")
	      f.input :store_description, :label => I18n.t("active_admin.store.store_description")
	      f.input :latitud, :label => I18n.t("active_admin.store.latitud")
	      f.input :longitud, :label => I18n.t("active_admin.store.longitud")
	      f.input :category, :label => I18n.t("active_admin.store.category"), :collection => Category.collection #, :include_blank => false
	      f.input :status, :label => I18n.t("active_admin.store.active") if !f.object.new_record?
	      f.input :logo, :label => I18n.t("active_admin.store.logo"), as: :file
	      f.input :expireDate, :label => I18n.t("active_admin.store.expireDate"), :start_year => Time.now.year
	    end
	    f.inputs I18n.t("active_admin.store.branch_offices") do
		    f.has_many :branch_offices do |bo|
		      bo.input :title, :label => false, :placeholder => I18n.t("active_admin.store.title")
		      bo.input :latitud, :label => false, :placeholder => I18n.t("active_admin.store.latitud")
		      bo.input :longitud, :label => false, :placeholder => I18n.t("active_admin.store.longitud")
		      bo.input :_destroy, :as=>:boolean, :required => false, :label => I18n.t('active_admin.store.delete_branch_office') unless bo.object.new_record?
		    end
		end
	    f.actions
  	end
end