ActiveAdmin.register_page "Dashboard" do
  menu priority: 0, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    

    controller do
        def index
            flash.keep
        end
    end
    #Here is an example of a simple dashboard with columns and panels.
    

    columns do
        div class: "column-container" do
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_created_user" do
                        panel "Usuarios creados recientemente" do
                            div do
                                ul do
                                    User.last(5).reverse.map do |user|
                                        li :style=>"list-style: none; text-align: right" do
                                            link_to("#{user.email[/[^@]+/]} - <strong>#{user.created_at.to_date}</strong>".html_safe, admin_user_path(user))
                                        end
                                        hr
                                    end
                                end
                                br
                                div class: "action_item" do
                                    link_to("Nuevo(a) Usuario", new_admin_user_path(), :class => "dashboard_action")
                                end
                            end
                        end
                    end
                end
            end
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_access" do
                        panel "Últimos Accesos Administrativos" do
                            div do
                                ul do
                                    AdminUser.recent_auth_access.map do |admin_user|
                                        if admin_user.current_sign_in_at.present?
                                            li :style=>"list-style: none; text-align: right" do
                                                    "#{admin_user.email[/[^@]+/]} - <strong>#{localize(admin_user.current_sign_in_at, :format => :long)}</strong>".html_safe
                                            end
                                            hr
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
        div class: "column-container" do
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_app_access" do
                        panel "Últimos Accesos APP " do
                            div do
                                ul do
                                    User.recent_auth_access.map do |user|
                                        if user.current_sign_in_at.present? 
                                            li :style=>"list-style: none; text-align: right" do
                                                "#{user.email[/[^@]+/]} - <strong>#{localize(user.current_sign_in_at, :format => :long)}</strong>".html_safe
                                            end
                                            hr
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_red" do
                        panel "Vencimientos" do
                            div do
                                ul do
                                    stores = Store.expireList
                                    if stores.present? 
                                        stores.map do |store|
                                            if store.expireDate.present?
                                                li :style=>"list-style: none; text-align: right" do
                                                    "#{store.title} - <strong>#{localize(store.expireDate, :format => :long)}</strong>".html_safe
                                                end
                                                hr
                                            end
                                        end
                                    else
                                        li :style=>"list-style: none; text-align: center" do
                                            "<strong>0</strong> comercios por vencer".html_safe
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    columns do
        div class: "column-container" do
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_app_user" do
                        panel "GreenPass APP Stats" do
                            div do
                                ul do
                                    li :style=>"list-style: none; text-align: right"do
                                        "Usuarios autenticados: <strong>#{User.loggedInUsers}</strong>".html_safe
                                    end
                                    hr
                                    li :style=>"list-style: none; text-align: right"do
                                        "Usuarios activos: <strong>#{User.activeUsers}</strong>".html_safe
                                    end
                                    hr
                                    li :style=>"list-style: none; text-align: right"do
                                        "Usuarios no activos: <strong>#{User.noActiveUsers}</strong>".html_safe
                                    end
                                end
                            end
                        end
                    end
                end
            end
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_category" do
                        panel "Top Categoría" do
                            div do
                                ul do
                                    categories = Category.top_categories
                                    if categories.present? 
                                        categories.map do |category|
                                            li :style=>"list-style: none; text-align: right" do
                                                "#{category.title}<br /><strong>#{category.count} Visitas</strong>".html_safe
                                            end
                                            hr
                                        end
                                    else
                                        li :style=>"list-style: none; text-align: center" do
                                            "<strong>0</strong> categorías visitadas".html_safe
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_promotion" do
                        panel "Top Promociones" do
                            div do
                                ul do
                                    promotions = Promotion.top_promotions
                                    if promotions.present? 
                                        promotions.map do |promotion|
                                            li :style=>"list-style: none; text-align: right" do
                                                "#{promotion.store.title} - #{promotion.title}<br /><strong>#{promotion.count} Visitas</strong>".html_safe
                                            end
                                            hr
                                        end
                                    else
                                        li :style=>"list-style: none; text-align: center" do
                                            "<strong>0</strong> promociones visitadas".html_safe
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
            div class: "column" do
                div class: "blank_slate_container" do
                    div class: "blank_slate header_inbox" do
                        panel "Inbox" do
                            div do
                                ul do
                                    feed_backs = FeedBack.last_comments
                                    if feed_backs.present? 
                                        feed_backs.map do |feed_back|
                                            li :style=>"list-style: none; text-align: right" do
                                                "<a href='#{admin_feed_back_path(feed_back.id)}'>#{feed_back.user.email[/[^@]+/]}</a> - <strong>#{localize(feed_back.created_at, :format => :long)}</strong>".html_safe
                                            end
                                            hr
                                        end
                                    else
                                        li :style=>"list-style: none; text-align: center" do
                                            "<strong>0</strong> comentarios sin leer".html_safe
                                        end
                                    end
                                end
                                br
                                div class: "action_item" do
                                    link_to("Inbox", admin_feed_backs_path(), :class => "dashboard_action", :id => "display-filter")
                                end
                            end
                        end
                    end
                end
            end
        end

        

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    end
  end # content
end
