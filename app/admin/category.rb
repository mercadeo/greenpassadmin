ActiveAdmin.register Category do
	menu id:"category", label: proc{ I18n.t("active_admin.category.display_name") }, parent: "Administrador", priority: 0
	permit_params :title, :description, :status, :image
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
	filter :title
	filter :description
	filter :status, label: I18n.t("active_admin.category.published")

	index do
		#selectable_column
		#id_column
		column I18n.t("active_admin.category.title"), :title
		column I18n.t("active_admin.category.description"), :description
		column I18n.t("active_admin.category.published"), :status
	actions
	end

	show do
		panel "Category" do
	      attributes_table_for category do
	        row(I18n.t("active_admin.category.title")) { |category| category.title}
	        row(I18n.t("active_admin.category.description")) { |category| category.description}
	        row(I18n.t("active_admin.category.image")) { |category| image_tag(category.image_source, :style=>"max-width: 300px")}
	      	row(I18n.t("active_admin.category.published")) { |b| status_tag b.is_published? }
	      end
	    end
	end
	#sidebar "Image", only: :show do
	#    attributes_table_for category do
	#      row :image do
	#        image_tag category.image.url, :style=>"width: 100%"
	#      end
	      #row('Published?') { |b| status_tag b.published? }
	#    end
  	#end
	form do |f|
		f.semantic_errors *f.object.errors.keys
	    f.inputs "Category Details" do
	      f.input :title, :label => I18n.t("active_admin.category.title")
	      f.input :description, :label => I18n.t("active_admin.category.description")
	      f.input :status, :label => I18n.t("active_admin.category.published") if !f.object.new_record?
	      f.input :image, :label => I18n.t("active_admin.category.image"), :as => :file
	    end
	    f.actions
  	end
end
