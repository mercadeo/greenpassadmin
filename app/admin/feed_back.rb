ActiveAdmin.register FeedBack do
	menu label: proc{ I18n.t("active_admin.feed_back.display_name") }
	permit_params :title, :description, :read
	filter :user , label: I18n.t("active_admin.feed_back.user")
	filter :read, label: I18n.t("active_admin.feed_back.read")
	controller do
		def show
			@feed_back = FeedBack.find(params[:id])
			@feed_back.read = true
			@feed_back.save
		end
		def edit 
			@feed_back = FeedBack.find(params[:id])
			@feed_back.read = true
			@feed_back.save
		end
	
		def scoped_collection
			super.includes :user # prevents N+1 queries to your database
		end
	end 
	index do
		column :user, sortable: "users.email" do |feed_back|
	    	feed_back.user.email if feed_back.user.present?
	    end 
		column I18n.t("active_admin.feed_back.title"), :title
		column I18n.t("active_admin.feed_back.read"), :read
		column I18n.t("active_admin.feed_back.created_at"), :created_at
	actions
	end
	show do
		panel "Comentarios" do
	      attributes_table_for feed_back do
	        row(I18n.t("active_admin.feed_back.user")) { |feed_back| "#{feed_back.user.user_attribute.first_name} #{feed_back.user.user_attribute.last_name}" }
	        row(I18n.t("active_admin.feed_back.title")) { |feed_back| feed_back.title}
	        row(I18n.t("active_admin.feed_back.description")) { |feed_back| feed_back.description}
	      end
	    end
	end
end