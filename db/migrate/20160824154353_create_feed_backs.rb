class CreateFeedBacks < ActiveRecord::Migration
  def change
    create_table :feed_backs do |t|
      t.references :user, index: true
      t.string :title
      t.text :description
      t.boolean :read

      t.timestamps
    end
  end
end
