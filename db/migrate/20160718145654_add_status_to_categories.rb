class AddStatusToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :status, :boolean, :default=> true
  end
end
