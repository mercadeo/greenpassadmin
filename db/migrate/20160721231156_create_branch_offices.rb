class CreateBranchOffices < ActiveRecord::Migration
  def change
    create_table :branch_offices do |t|
      t.string :title
      t.decimal :latitud, index:true
      t.decimal :longitud, index:true
      t.references :store, index: true

      t.timestamps
    end
  end
end
