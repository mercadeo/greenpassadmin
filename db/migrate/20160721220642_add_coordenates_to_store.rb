class AddCoordenatesToStore < ActiveRecord::Migration
  def change
    add_column :stores, :latitud, :decimal
    add_column :stores, :longitud, :decimal
  end
end
