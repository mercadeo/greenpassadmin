class AddDefaultReadValueToFeedBacks < ActiveRecord::Migration
  def change
  	change_column :feed_backs, :read, :boolean, :default => false
  end
end
