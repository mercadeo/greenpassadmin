class AddCompanyNameToUserAttributes < ActiveRecord::Migration
  def change
    add_column :user_attributes, :company_name, :string
    add_index :user_attributes, :company_name
  end
end
