class AddIsLoggedInToUsers < ActiveRecord::Migration
  def change
    add_column :users, :isLoggedIn, :boolean, :default => false
  end
end
