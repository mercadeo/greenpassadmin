class AddCountToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :count, :integer, :default => 0
  end
end
