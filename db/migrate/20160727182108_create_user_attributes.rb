class CreateUserAttributes < ActiveRecord::Migration
  def change
    create_table :user_attributes do |t|
      t.string :first_name
      t.string :last_name
      t.string :carnet, index: true
      t.string :cedula, index: true
      t.date :employee_start_date, index: true
      t.references :user, index: true

      t.timestamps
    end
  end
end
