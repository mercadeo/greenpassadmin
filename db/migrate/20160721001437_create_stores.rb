class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :title
      t.text :store_description
      t.references :category, index: true
      t.string :logo

      t.timestamps
    end
  end
end
