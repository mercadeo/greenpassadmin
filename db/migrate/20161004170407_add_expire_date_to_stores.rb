class AddExpireDateToStores < ActiveRecord::Migration
  def change
    add_column :stores, :expireDate, :date
  end
end
