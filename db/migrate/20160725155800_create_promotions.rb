class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.string :title
      t.text :description
      t.integer :discount
      t.text :conditions
      t.string :image
      t.date :start_date, index: true
      t.date :end_date, index: true
      t.references :store, index: true

      t.timestamps
    end
  end
end
