class AddStatusToStore < ActiveRecord::Migration
  def change
    add_column :stores, :status, :boolean, :default=> true
  end
end
